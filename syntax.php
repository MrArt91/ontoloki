<?php
/**
 * DokuWiki Plugin ontoloki (Syntax Component)
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  Micha&#322; Lenart <mrart@student.agh.edu.pl>
 */

// must be run within Dokuwiki
if (!defined('DOKU_INC')) die();

require_once("config.php");

class syntax_plugin_ontoloki extends DokuWiki_Syntax_Plugin {
    /**
     * @return string Syntax mode type
     */
    public function getType() {
        return 'substition';
    }
    /**
     * @return string Paragraph type
     */
    public function getPType() {
        return 'normal';
    }
    /**
     * @return int Sort order - Low numbers go before high numbers
     */
    public function getSort() {
        return 500;
    }

    /**
     * Connect lookup pattern to lexer.
     *
     * @param string $mode Parser mode
     */
    public function connectTo($mode) {
        $this->Lexer->addSpecialPattern('<ontoloki>',$mode,'plugin_ontoloki');
//        $this->Lexer->addEntryPattern('<FIXME>',$mode,'plugin_ontoloki');
    }

//    public function postConnect() {
//        $this->Lexer->addExitPattern('</FIXME>','plugin_ontoloki');
//    }

    /**
     * Handle matches of the ontoloki syntax
     *
     * @param string $match The match of the syntax
     * @param int    $state The state of the handler
     * @param int    $pos The position in the document
     * @param Doku_Handler    $handler The handler
     * @return array Data for the renderer
     */
    public function handle($match, $state, $pos, Doku_Handler &$handler){
        $data = array();

        return $data;
    }

    /**
     * Render xhtml output or metadata
     *
     * @param string         $mode      Renderer mode (supported modes: xhtml)
     * @param Doku_Renderer  $renderer  The renderer
     * @param array          $data      The data from the handler() function
     * @return bool If rendering was successful.
     */
    public function render($mode, Doku_Renderer &$renderer, $data) {
        if($mode != 'xhtml') return false;

        // Set useful paths.
        $graphTxtPath = OL_PLUGINS.'/tmp/ontoloki/graph.txt';
        $graphSvgPath = OL_PLUGINS.'/tmp/ontoloki/graph.svg';
        $graphPlPath = OL_PLUGINS.'/ontoloki/graph.pl';

        $plResult = $this->call_prolog($graphPlPath, $this->prepareGoal());

        // Convert page names to html links
        $plResultWithLinks = $this->convertWikiLinks($plResult);

        // Save graph as .txt
        io_saveFile($graphTxtPath, $plResultWithLinks);

        // Execute graphviz
        $graphSvg = shell_exec($this->getConf('layout_command').' -Tsvg '.$graphTxtPath);
        
        // Set id attribute to svg element and display graph. This ID is required for JavaScript part
        // to properly move and resize graph interactively
        $renderer->doc .= $this->setSvgId($graphSvg, "ontology-graph-svg");

        // Append JavaScript code to enable interactive graph.
        $renderer->doc .= '<script type="text/javascript">'.io_readFile(OL_PLUGINS.'/ontoloki/append.js').'</script>';

        return true;
    }

    /**
     * Set id attribute to first <svg> tag
     *
     * @param   string  $svg  SVG image
     * @param   string  $id   ID that will be inserted
     * @return string SVG with added ID attribute
     */
    private function setSvgId($svg, $id) {
        return preg_replace('/<svg/', '<svg id="'.$id.'"', $svg, 1);
    }

    /**
     * Converts wiki page name within "%%" to HTML links
     *
     * @param  string  $doc  Document in which links will be converted
     */
    private function convertWikiLinks($doc) {
        return preg_replace_callback('/%%(.*?)%%/',
            function ($matchPage) {
                return DOKU_URL.'doku.php?id='.$matchPage[1];
            },
            $doc);
    }

    /**
     * Invokes Prolog engine from "Loki" plugin with appended file and specific goal
     *
     * @param  string  $file  Path to file that will be appended to default Loki Prolog script
     * @param  string  $goal  Prolog goal
     * @return string Output of Loki Prolog execution
     */
    private function call_prolog($file, $goal) {
        require_once(OL_PLUGINS.'/loki/utl/loki_utl.php');

        $lokiUtl = new LokiUtl;

        $temp_dir = OL_PLUGINS.'/tmp/loki';
        $hash = 'ontograph';

        exec('echo ":- style_check(-discontiguous)." > '.$temp_dir.'dokuwiki.code'.$hash);
        exec('grep ".*" -rh  $(grep -rl ".*" '. $temp_dir.' | grep "'.$scope.'")  >> '.$temp_dir.'dokuwiki.code'.$hash);
        exec('cat '.$file.' >> '.$temp_dir.'dokuwiki.code'.$hash);

        return $lokiUtl->call_prolog($goal, '*', $hash);
    }

    /**
     * Prepares Prolog goal, which contains assertion of GraphViz configuration and "write_ontograph" clause
     *
     * @return Prolog goal
     */
    private function prepareGoal() {
        $assert_conf = function ($name) {
            $value = $this->getConf($name);
            return 'assert('.$name."('".$value."'))";
        };

        $goal = array();
        
        $goal[] = $assert_conf("category_edge_attrs");
        $goal[] = $assert_conf("relation_edge_attrs");
        $goal[] = $assert_conf("attribute_edge_attrs");
        $goal[] = $assert_conf("page_node_attrs");
        $goal[] = $assert_conf("category_node_attrs");
        $goal[] = $assert_conf("attribute_node_attrs");

        $goal[] = "write_ontograph";

        return (join(", ", $goal));
    }
}

// vim:ts=4:sw=4:et:
