(function($) {
    $(document).ready(function() {
        var svg = $('#ontology-graph-svg');
        svg.removeAttr('viewBox');
        svg.attr('width', "100%");
        svg.svgPan('ontology-graph', true, true, false, 0.2);
    });
})(jQuery);
