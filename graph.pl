write_category_edge(N1, N2) :-
    category_edge_attrs(Attrs), !,
    writef('"%w" -> "%w" [ label = category %w ];', [N1, N2, Attrs]).

write_relation_edge(N1, N2, Label) :-
    relation_edge_attrs(Attrs), !,
    writef('"%w" -> "%w" [ label = "%w" %w ];', [N1, N2, Label, Attrs]).

write_attribute_edge(N1, N2, Label) :-
    attribute_edge_attrs(Attrs), !,
    writef('"%w" -> "%w" [ label = "%w" %w ];', [N1, N2, Label, Attrs]).

write_page_node(N) :-
    page_node_attrs(Attrs), !,
    writef('"%w" [ URL="%%%w%%" %w ]', [N, N, Attrs]). 

write_category_node(N) :-
    category_node_attrs(Attrs), !,
    writef('"%w" [ URL="%%%w%%" %w ]', [N, N, Attrs]). 

write_attribute_node(N) :-
    attribute_node_attrs(Attrs), !,
    writef('"%w" [ URL="%%%w%%" %w ]', [N, N, Attrs]). 

list_all_page_nodes :-
    wiki_category(Page, _),
    write_page_node(Page), write(';'), nl,
    fail.
list_all_page_nodes.

list_all_category_nodes :-
    wiki_category(_, Category),
    write_category_node(Category), write(';'), nl,
    fail.
list_all_category_nodes.

list_all_attribute_nodes :-
    wiki_category(Page, _),
    wiki_attribute(Page, _, Value),
    write_attribute_node(Value), write(';'), nl,
    fail.
list_all_attribute_nodes.

list_all_category_edges :-
    wiki_category(Page, Category),
    write_category_edge(Page, Category), nl,
    fail.
list_all_category_edges.

list_all_relation_edges :-
    wiki_relation(Child, Relation, Parent),
    write_relation_edge(Child, Parent, Relation), nl,
    fail.
list_all_relation_edges.

list_all_attribute_edges :-
    wiki_category(Page, _),
    wiki_attribute(Page, Attribute, Value),
    write_attribute_edge(Page, Value, Attribute), nl,
    fail.
list_all_attribute_edges.

write_ontograph :-
    write('digraph ontograph {'), nl,
    write('    id="ontology-graph";'), nl,
    list_all_page_nodes, nl,
    list_all_category_nodes, nl,
    list_all_attribute_nodes, nl,
    list_all_category_edges, nl,
    list_all_relation_edges, nl,
    list_all_attribute_edges, nl,
    write('}'), nl.
