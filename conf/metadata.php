<?php
$meta["layout_command"] = array('multichoice', '_choices'
                            => array('circo', 'dot', 'fdp', 'neato', 'osage', 'sfdp', 'twopi'));
$meta["category_edge_attrs"] = array('string');
$meta["relation_edge_attrs"] = array('string');
$meta["attribute_edge_attrs"] = array('string');
$meta["page_node_attrs"] = array('string');
$meta["category_node_attrs"] = array('string');
$meta["attribute_node_attrs"] = array('string');
