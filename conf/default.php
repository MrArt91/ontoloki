<?php
$conf["layout_command"] = "dot";
$conf["category_edge_attrs"] = 'style=dashed';
$conf["relation_edge_attrs"] = '';
$conf["attribute_edge_attrs"] = 'style=dotted, fontcolor=gray25';
$conf["page_node_attrs"] = 'shape=ellipse, style=filled, fillcolor=coral';
$conf["category_node_attrs"] = 'shape=diamond, style=filled, fillcolor=steelblue1';
$conf["attribute_node_attrs"] = 'shape=rect, style=dotted, arrowhead=vee, color=gray16, fontcolor=gray16';
