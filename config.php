<?php

$def = function($name, $val) {
    if(!defined($name)) define($name, $val);
};

$def('OL_PLUGINS',   realpath(dirname(__FILE__).'/..'));
$def('OL_GRAPH_PL',  'ontograph.pl');
$def('OL_GRAPH_SVG', 'ontograph.svg');

unset($def);

?>
